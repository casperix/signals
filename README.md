# signals

---
## Examples
Signals without message:
```
val signal = EmptySignal()
signal.then {
    println("action")
}
println("call")
signal.set()
//  output:
//  > call
//  > action
```
Signal with a message of a concrete type:
```
val signal = Signal<String>()
signal.then {
    println("action: $it")
}
println("call")
signal.set("ping")
//  output:
//  > call
//  > action: ping
```
There is also a one-time signal:
```
val signal = SingleSignal<String>()
signal.then {
    println("action: $it")
}
println("call")
signal.set("first")
signal.set("second")
//  output:
//  > call
//  > action: first
```
Two-choice signal - promise (only one final option):
```
val signal = EitherSignal<Int, String>()
signal.thenAccept {
    println("accept: $it")
}
signal.thenReject {
    println("reject: $it")
}
println("call")
signal.accept(1)  //set final value
signal.reject("ignored") //ignore - the final value is already setted
//  output:
//  > call
//  > accept: 1
```
You can also observe a collection of items:
```
val items = ObservableMutableList<Int>()

items.add(1)

//  Starting the observation
items.thenAdd {
    println("add: $it")
}
items.thenRemove {
    println("remove: $it")
}

items.add(2)
items.remove(1)

//  output:
//  > add: 2
//  > remove: 1
```
You can build a chain of signals. A simple example is given. In fact, you can build an arbitrary tree:
```
val providerA = Signal<String>()
providerA.then { println("A->$it") }

val providerB = Signal<String>()
providerB.then { println("B->$it") }

val catcher = Signal.pipe(providerA, providerB)
catcher.then { println("Catcher->$it") }

providerA.set("hello")
providerB.set("world")

//	output :
//  > A->hello
//  > Catcher->hello
//  > B->world
//  > Catcher->world
```
---

## Deploy

### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator):
`sonatypeStagingProfileId`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.


Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin