1.8.2
- EitherFuture simple accept \ reject

1.8.1
- update dependencies

1.8.0
- use package `ru.casperix.signals`
- back to java 17

1.7.8
- auto deploy

1.6.0
-   StorageSignal "then" -- not call immediately. Use "thenAndNow" for callback now too.