package ru.casperix.signals

import ru.casperix.signals.concrete.Signal
import ru.casperix.signals.helper.Content
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

class PerformanceSignalTest {

	@Test
	@ExperimentalTime
	fun manyCalls() {
		val MAX = 1000_000
		val signal = Signal<Content>()
		val content = Content("hello")
		var callCounter = 0

		signal.then {
			callCounter++
		}

		val time = measureTime {
			for (i in 1..MAX) {
				signal.set(content)
			}
		}

//		assertEquals(0, 1)
		assertEquals(MAX, callCounter)
		println("manyCalls: $MAX iterations for $time")
	}

	@Test
	@ExperimentalTime
	fun manyListeners() {
		val MAX = 1000_000
		val signal = Signal<Content>()
		val content = Content("hello")
		var callCounter = 0

		val prepareTime = measureTime {
			for (i in 1..MAX) {
				signal.then {
					callCounter++
				}
			}
		}
		val time = measureTime {
			signal.set(content)
		}

		assertEquals(MAX, callCounter)
		println("manyListeners: $MAX iterations for $time (prepare: $prepareTime)")
	}
}