package ru.casperix.signals

import ru.casperix.signals.collection.ObservableMutableList
import ru.casperix.signals.concrete.EitherSignal
import ru.casperix.signals.concrete.EmptySignal
import ru.casperix.signals.concrete.Signal
import ru.casperix.signals.concrete.SingleSignal
import kotlin.test.Test

class Examples {
    @Test
    fun emptySignal() {
        val signal = EmptySignal()
        signal.then {
            println("action")
        }
        println("call")
        signal.set()
        //  output:
        //  > call
        //  > action
    }

    @Test
    fun valueSignal() {
        val signal = Signal<String>()
        signal.then {
            println("action: $it")
        }
        println("call")
        signal.set("ping")
        //  output:
        //  > call
        //  > action: ping
    }

    @Test
    fun singleSignal() {
        val signal = SingleSignal<String>()
        signal.then {
            println("action: $it")
        }
        println("call")
        signal.set("first")
        signal.set("second")
        //  output:
        //  > call
        //  > action: first
    }

    @Test
    fun eitherSignal() {
        val signal = EitherSignal<Int, String>()
        signal.thenAccept {
            println("accept: $it")
        }
        signal.thenReject {
            println("reject: $it")
        }
        println("call")
        signal.accept(1)
        //  output:
        //  > call
        //  > accept: 1
    }

    @Test
    fun observableCollection() {
        val items = ObservableMutableList<Int>()

        items.add(1)

        //  Starting the observation
        items.thenAdd {
            println("add: $it")
        }
        items.thenRemove {
            println("remove: $it")
        }

        items.add(2)
        items.remove(1)

        //  output:
        //  > add: 2
        //  > remove: 1
    }

    @Test
    fun pipe() {
        val providerA = Signal<String>()
        providerA.then { println("A->$it") }

        val providerB = Signal<String>()
        providerB.then { println("B->$it") }

        val catcher = Signal.pipe(providerA, providerB)
        catcher.then { println("Catcher->$it") }

        providerA.set("hello")
        providerB.set("world")

        //	output :
        //  > A->hello
        //  > Catcher->hello
        //  > B->world
        //  > Catcher->world
    }
}