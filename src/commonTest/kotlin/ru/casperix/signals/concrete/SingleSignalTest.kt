package ru.casperix.signals.concrete

import ru.casperix.signals.concrete.SingleSignal
import kotlin.test.Test
import kotlin.test.assertEquals

class SingleSignalTest {
	@Test
	fun testSet() {
		var results = ""
		val single = SingleSignal<String>()

		single.then { results += ("1-$it;") }
		single.set("hello")
		single.then { results += ("2-$it;") }
		single.set("ignored")
		single.then { results += ("3-$it;") }

		single.removeAllListeners()
		single.set("ignored too")
		single.then { results += ("4-$it;") }


		assertEquals("1-hello;2-hello;3-hello;4-hello;", results)
	}
}