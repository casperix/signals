package ru.casperix.signals.concrete

import ru.casperix.signals.helper.CustomItem
import kotlin.test.*

class SignalTest {
	private val resultsA = mutableListOf<Int>()
	private val resultsB = mutableListOf<Int>()

	@BeforeTest
	fun init() {
		resultsA.clear()
		resultsB.clear()
	}

	@Test
	fun testThen() {
		val signal = Signal<Int>()
		val listener: (Int) -> Unit = { resultsA.add(it) }
		val slot = signal.then(listener)

		assertEquals(0, resultsA.size)
		assertEquals(1, signal.size)
		assertEquals(listener, signal.getListener(slot))
	}

	@Test
	fun testCancel() {
		val signal = Signal<Int>()
		val listener: (Int) -> Unit = { resultsA.add(it) }
		val slot = signal.then(listener)
		signal.cancel(slot)

		assertEquals(0, resultsA.size)
		assertEquals(0, signal.size)
	}

	@Test
	fun testClear() {
		val signal = Signal<Int>()
		val listener: (Int) -> Unit = { resultsA.add(it) }
		signal.then(listener)
		signal.removeAllListeners()

		assertEquals(0, resultsA.size)
		assertEquals(0, signal.size)
	}

	@Test
	fun testCancelOther() {
		val signal = Signal<Int>()
		val signalOther = Signal<Int>()
		val listener: (Int) -> Unit = { resultsA.add(it) }
		val listenerOther: (Int) -> Unit = { resultsB.add(it) }
		val slot = signal.then(listener)
		val slotOther = signalOther.then(listenerOther)
		signal.cancel(slotOther)

		assertEquals(listener, signal.getListener(slot))
		assertEquals(listenerOther, signalOther.getListener(slotOther))
	}

	@Test
	fun testSet() {
		val signal = Signal<Int>()
		signal.then { resultsA.add(it) }
		signal.set(11)

		assertEquals(1, resultsA.size)
		assertEquals(11, resultsA[0])
	}

	@Test
	fun testSetComplex() {
		val signal = Signal<Int>()
		val listener: (Int) -> Unit = { resultsA.add(it) }
		val slot = signal.then(listener)
		signal.set(0)
		signal.set(1)
		signal.set(2)
		signal.set(3)

		signal.cancel(slot)
		signal.set(88)

		signal.then(listener)
		signal.set(4)

		signal.removeAllListeners()
		signal.set(99)

		assertEquals(5, resultsA.size)
		for (i in 0..4) {
			assertEquals(i, resultsA[i])
		}
	}


	@Test
	fun instanceMethodCancel() {
		val itemA = CustomItem()
		val itemB = CustomItem()

		val otherSignal = Signal<Int>()
		val otherSlot = otherSignal.then(itemB::action)
		assertNotNull(otherSlot)

		val signal = Signal<Int>()

		val slotA = signal.then(itemA::action)
		signal.cancel(otherSlot)
		signal.set(22)
		assertTrue { itemA.results.size == 1 && itemA.results[0] == 22 }

		signal.cancel(slotA)
		signal.set(33)
		assertTrue { itemA.results.size == 1 && itemA.results[0] == 22 }

		val slotB = signal.then(itemB::action)
		assertNotNull(slotB)

		signal.cancel(slotA)
		signal.set(44)
		assertTrue { itemB.results.size == 1 && itemB.results[0] == 44 }
		assertTrue { itemA.results.size == 1 && itemA.results[0] == 22 }
	}

	private fun actionA(value: Int) {
		resultsA.add(value)
	}

	fun actionB(value: Int) {
		resultsB.add(value)
	}

	@Test
	fun selfMethodCancel() {
		val signal = Signal<Int>()

		val slotA = signal.then(::actionA)
		assertNotNull(slotA)

		signal.set(22)
		assertTrue { resultsA.size == 1 && resultsA[0] == 22 }

		signal.cancel(slotA)
		signal.set(33)
		assertTrue { resultsA.size == 1 && resultsA[0] == 22 }

		val slotB = signal.then(::actionB)
		assertNotNull(slotB)

		signal.cancel(slotA)
		signal.set(44)
		assertTrue { resultsB.size == 1 && resultsB[0] == 44 }
		assertTrue { resultsA.size == 1 && resultsA[0] == 22 }
	}
}