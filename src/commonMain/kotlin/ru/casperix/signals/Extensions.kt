package ru.casperix.signals

import ru.casperix.signals.collection.ObservableCollection
import ru.casperix.signals.collection.ObservableMap
import ru.casperix.misc.Disposable
import ru.casperix.signals.concrete.*


fun <Source, Target> Signal<Source>.map(convert:(Source)->Target): Signal<Target> {
	val other = Signal<Target>()
	then {
		other.set(convert(it))
	}
	return other
}

fun <Key, Value> ObservableMap<Key, Value>.then(holder: MutableCollection<in Disposable>, forAdd: (Map.Entry<Key, Value>) -> Unit, forRemove: (Map.Entry<Key, Value>) -> Unit) {
	thenAdd(forAdd)?.let { holder.add(it) }
	thenRemove(forRemove)?.let { holder.add(it) }
}


fun <Key, Value> ObservableMap<Key, Value>.thenAdd(holder: MutableCollection<in Disposable>, listener: (Map.Entry<Key, Value>) -> Unit) {
	 addFuture().then(holder, listener)
}

fun <Key, Value> ObservableMap<Key, Value>.thenRemove(holder: MutableCollection<in Disposable>, listener: (Map.Entry<Key, Value>) -> Unit) {
	removeFuture().then(holder, listener)
}

fun <Key, Value> ObservableMap<Key, Value>.thenAddKey(holder: MutableCollection<in Disposable>, listener: (Key) -> Unit) {
	addFuture().then(holder) { listener(it.key) }
}

fun <Key, Value> ObservableMap<Key, Value>.thenRemoveKey(holder: MutableCollection<in Disposable>, listener: (Key) -> Unit) {
	removeFuture().then(holder) { listener(it.key) }
}

fun <Key, Value> ObservableMap<Key, Value>.thenAddValue(holder: MutableCollection<in Disposable>, listener: (Value) -> Unit) {
	addFuture().then(holder) { listener(it.value) }
}

fun <Key, Value> ObservableMap<Key, Value>.thenRemoveValue(holder: MutableCollection<in Disposable>, listener: (Value) -> Unit) {
	removeFuture().then(holder) { listener(it.value) }
}

fun <T> ObservableCollection<T>.then(holder: MutableCollection<in Disposable>, forAdd: (T) -> Unit, forRemove: (T) -> Unit) {
	thenAdd(forAdd)?.let { holder.add(it) }
	thenRemove(forRemove)?.let { holder.add(it) }
}

fun <T> ObservableCollection<T>.thenAdd(holder: MutableCollection<in Disposable>, forAdd: (T) -> Unit) {
	thenAdd(forAdd)?.let { holder.add(it) }
}

fun <T> ObservableCollection<T>.thenRemove(holder: MutableCollection<in Disposable>, forRemove: (T) -> Unit) {
	thenRemove(forRemove)?.let { holder.add(it) }
}

fun <Event> Future<Event>.then(holder: MutableCollection<in Disposable>, listener: (Event) -> Unit) {
	holder.add(then(listener))
}

fun <Event> UpdateFuture<Event>.then(holder: MutableCollection<in Disposable>, listener: (Event, Event) -> Unit) {
	holder.add(then(listener))
}

fun EmptyFuture.then(holder: MutableCollection<in Disposable>, listener: () -> Unit) {
	holder.add(then(listener))
}

fun EmptyEitherFuture.then(holder: MutableCollection<in Disposable>, onAccept: () -> Unit, onReject: () -> Unit) {
	acceptFuture.then(holder, onAccept)
	rejectFuture.then(holder, onReject)
}

fun EmptyEitherFuture.thenAccept(holder: MutableCollection<in Disposable>, onAccept: () -> Unit) {
	acceptFuture.then(holder, onAccept)
}

fun EmptyEitherFuture.thenReject(holder: MutableCollection<in Disposable>, onReject: () -> Unit) {
	rejectFuture.then(holder, onReject)
}

fun <Accept, Reject> EitherFuture<Accept, Reject>.then(holder: MutableCollection<in Disposable>, onAccept: (Accept) -> Unit, onReject: (Reject) -> Unit) {
	acceptFuture.then(holder, onAccept)
	rejectFuture.then(holder, onReject)
}

fun <Accept, Reject> EitherFuture<Accept, Reject>.thenAccept(holder: MutableCollection<in Disposable>, onAccept: (Accept) -> Unit) {
	acceptFuture.then(holder, onAccept)
}

fun <Accept, Reject> EitherFuture<Accept, Reject>.thenReject(holder: MutableCollection<in Disposable>, onReject: (Reject) -> Unit) {
	rejectFuture.then(holder, onReject)
}

fun BooleanSwitcher.switch() {
	value = !value
}