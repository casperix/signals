package ru.casperix.signals.api

interface CustomStorage<Event> {
	var value: Event
}