package ru.casperix.signals.api

interface CustomSinglePromise<Listener, Slot> : CustomPromise<Listener, Slot>, CustomSingle