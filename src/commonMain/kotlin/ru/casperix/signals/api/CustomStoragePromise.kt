package ru.casperix.signals.api

interface CustomStoragePromise<Listener, Event, Slot> : CustomPromise<Listener, Slot>, CustomStorageFuture<Listener, Event, Slot>