package ru.casperix.signals.api

interface CustomStorageFuture<Listener, Event, Slot> : CustomFuture<Listener, Slot> {
	val value: Event
}
