package ru.casperix.signals.api

abstract class AbstractSignal<Listener, Slot>(val slots: CustomSlotCollection<Listener, Slot>) : CustomSlotCollection<Listener, Slot> by slots,
    CustomPromise<Listener, Slot>