package ru.casperix.signals.api


interface CustomEitherPromise<AcceptListener, RejectListener, Slot> : CustomEitherFuture<AcceptListener, RejectListener, Slot> {
    val acceptDispatcher: AcceptListener
    val rejectDispatcher: RejectListener
}