package ru.casperix.signals.api


interface CustomFuture<Listener, Slot> {
	fun then(listener: Listener): Slot
	fun cancel(slot: Slot): Boolean

}