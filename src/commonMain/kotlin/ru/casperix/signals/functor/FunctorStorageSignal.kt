package ru.casperix.signals.functor

import ru.casperix.signals.api.AbstractSignal

class FunctorStorageSignal<Listener : Functor<Event>, Event>(first: Event, val ignoreEqual: Boolean = true, val dispatchOnThen: Boolean = true) : FunctorStoragePromise<Listener, Event>, AbstractSignal<Listener, Listener>(
    FunctorCollection<Listener, Event>()
) {
	override var value: Event = first
		set(value) {
			if (!ignoreEqual || field != value) {
				field = value

				forEach { listener ->
					listener.receiver(value)
				}
			}
		}

	override fun then(listener: Listener): Listener {
		if (dispatchOnThen) {
			listener.receiver(value)
		}
		return super.then(listener)
	}

	override fun send(value: Event) {
		this.value = value
	}
}