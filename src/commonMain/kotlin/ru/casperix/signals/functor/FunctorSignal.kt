package ru.casperix.signals.functor

import ru.casperix.signals.api.AbstractSignal

class FunctorSignal<Listener : Functor<Event>, Event> : FunctorPromise<Listener, Event>, AbstractSignal<Listener, Listener>(FunctorCollection<Listener, Event>()) {
	override fun send(value: Event) {
		forEach { listener ->
			listener.receiver(value)
		}
	}

}