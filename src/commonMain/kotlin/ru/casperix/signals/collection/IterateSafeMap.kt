package ru.casperix.signals.collection

class IterateSafeMap<Key, Value> : AbstractMap<Key, Value>() {
	private var map = LinkedHashMap<Key, Value>(4, 0.25f)
	private var locked = false

	fun forEach(action:(Map.Entry<Key, Value>)->Unit) {
		locked = true
		map.forEach (action)
		locked = false
	}

	fun forEachKey(action:(Key)->Unit) {
		locked = true
		map.keys.forEach (action)
		locked = false
	}

	fun forEachValue(action:(Value)->Unit) {
		locked = true
		map.values.forEach (action)
		locked = false
	}

	fun clear() {
		if (locked) {
			map = LinkedHashMap()
			locked = false
		} else {
			map.clear()
		}
	}

	fun put(key: Key, value: Value):Value? {
		if (locked) {
			map = LinkedHashMap(map)
			locked = false
		}
		return map.put(key, value)
	}

	fun remove(key: Key): Value? {
		if (locked) {
			map = LinkedHashMap(map)
			locked = false
		}
		return map.remove(key)
	}

	operator fun set(key: Key, value: Value) {
		put(key, value)
	}

	override val entries: Set<Map.Entry<Key, Value>>
		get() = map.entries

}