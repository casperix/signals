package ru.casperix.signals.collection

import ru.casperix.signals.concrete.Slot
import ru.casperix.signals.concrete.Future


interface ObservableCollection<Item> : Collection<Item> {
	fun addFuture(): Future<Item>
	fun removeFuture(): Future<Item>

	fun then(addListener: (Item) -> Unit, removeListener: (Item) -> Unit): Pair<Slot?, Slot?> {
		return Pair(
				addFuture().then(addListener),
				removeFuture().then(removeListener)
		)
	}

	fun cancel(forAdd: Slot?, forRemove: Slot?) {
		forAdd?.let { addFuture().cancel(it) }
		forRemove?.let { removeFuture().cancel(it) }
	}

	fun thenAdd(listener: (Item) -> Unit): Slot? {
		return addFuture().then(listener)
	}

	fun thenRemove(listener: (Item) -> Unit): Slot? {
		return removeFuture().then(listener)
	}
}