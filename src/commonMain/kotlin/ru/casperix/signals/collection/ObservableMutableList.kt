package ru.casperix.signals.collection

import ru.casperix.signals.concrete.Future
import ru.casperix.signals.concrete.Signal

class ObservableMutableList<Item>(val uniqueItems:Boolean = false) : MutableCollection<Item>, ObservableCollection<Item>, MutableList<Item> {
	private val items = mutableListOf<Item>()

	private val addSignal = Signal<Item>()
	private val removeSignal = Signal<Item>()

	override fun addFuture(): Future<Item> {
		return addSignal
	}

	override fun removeFuture(): Future<Item> {
		return removeSignal
	}

	override fun add(index:Int, element: Item) {
		if (!uniqueItems) {
			items.add(index, element)
			addSignal.set(element)
			return
		}
		val lastIndex = items.indexOf(element)
		if (lastIndex == index) return

		items.add(index, element)
		if (lastIndex != -1) {
			items.removeAt(lastIndex)
		} else {
			addSignal.set(element)
		}
	}

	override fun add(element: Item): Boolean {
		if (uniqueItems && items.contains(element)) return false
		if (items.add(element)) {
			addSignal.set(element)
			return true
		}
		return false
	}

	override fun remove(element: Item): Boolean {
		if (items.remove(element)) {
			removeSignal.set(element)
			return true
		}
		return false
	}

	override fun addAll(elements: Collection<Item>): Boolean {
		var result = false
		for (element in elements) {
			val next = add(element)
			result = result || next
		}
		return result
	}

	override fun clear() {
		removeAll(items.toList())
	}

	override fun iterator(): MutableIterator<Item> {
		return items.iterator()
	}

	override fun removeAll(elements: Collection<Item>): Boolean {
		var result = false
		for (element in elements) {
			val next = remove(element)
			result = result || next
		}
		return result
	}

	override fun retainAll(elements: Collection<Item>): Boolean {
		throw Error("No implementation")
	}

	override val size: Int
		get() = items.size

	override fun addAll(index: Int, elements: Collection<Item>): Boolean {
		val alreadyContains = items.containsAll(elements)
		if (alreadyContains) return false
		elements.forEach {
			add(index, it)
		}
		return true
	}

	override fun contains(element: Item): Boolean {
		return items.contains(element)
	}

	override fun containsAll(elements: Collection<Item>): Boolean {
		return items.containsAll(elements)
	}

	override fun isEmpty(): Boolean {
		return items.isEmpty()
	}

	override fun get(index: Int): Item {
		return items[index]
	}

	override fun indexOf(element: Item): Int {
		return items.indexOf(element)
	}

	override fun lastIndexOf(element: Item): Int {
		return items.lastIndexOf(element)
	}

	override fun listIterator(): MutableListIterator<Item> {
		return items.listIterator()
	}

	override fun listIterator(index: Int): MutableListIterator<Item> {
		return items.listIterator(index)
	}

	override fun removeAt(index: Int): Item {
		val element = items.removeAt(index)
		removeSignal.set(element)
		return element
	}

	override fun set(index: Int, element: Item): Item {
		val last = items.set(index, element)
		removeSignal.set(last)
		addSignal.set(element)
		return last
	}

	override fun subList(fromIndex: Int, toIndex: Int): MutableList<Item> {
		return items.subList(fromIndex, toIndex)
	}
}