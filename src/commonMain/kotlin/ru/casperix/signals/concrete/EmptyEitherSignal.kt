package ru.casperix.signals.concrete

import ru.casperix.signals.api.CustomFuture

/**
 *		Сигнал ожидания события "Успех" / "Неудача"
 * 	Аналогичен [EitherSignal]
 *		Однако сигнал не содержит сообщение
 *
 * 	val loadSignal = EmptyEitherSignal()
 * 	...
 * 	loadSignal.accept()
 * 	loadSignal.reject()
 * 	...
 * 	loadSignal.thenAccept{
 * 		println("accepted")
 * 	}
 * 	loadSignal.thenReject {
 * 		println("rejected")
 * 	}
 */
class EmptyEitherSignal : EmptyEitherPromise {
	private val acceptSignal = EmptySingleSignal()
	private val rejectSignal = EmptySingleSignal()
	override var complete = false; private set

	override fun accept() {
		if (!complete) {
			complete = true
			acceptSignal.set()
		}
	}

	override fun reject() {
		if (!complete) {
			complete = true
			rejectSignal.set()
		}
	}

	fun dropResult() {
		acceptSignal.dropResult()
		rejectSignal.dropResult()
		complete = false
	}

	override val acceptDispatcher: () -> Unit = ::accept

	override val rejectDispatcher: () -> Unit = ::reject

	override val acceptFuture: CustomFuture<() -> Unit, Slot> = acceptSignal

	override val rejectFuture: CustomFuture<() -> Unit, Slot> = rejectSignal
}