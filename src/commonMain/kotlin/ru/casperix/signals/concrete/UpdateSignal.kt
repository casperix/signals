package ru.casperix.signals.concrete

import ru.casperix.signals.api.AbstractSignal

/**
 * 	Аналогичен [StorageSignal]
 * 	Показывает старое значение
 * 	Не срабатывает при подписке. 
 * 	Игнорирует эквивалентные значения
 */
class UpdateSignal<Event>(first: Event) : AbstractSignal<(Event, Event) -> Unit, Slot>(SlotCollection()), UpdatePromise<Event> {
	var value: Event = first
		set(value: Event) {
			val last = field
			if (last == value) return
			field = value

			forEach { listener ->
				listener(last, value)
			}
		}

	override fun then(listener: (Event, Event) -> Unit): Slot {
		return super.then(listener)
	}

//	override fun get(): Event {
//		return value
//	}

	override fun set(value: Event) {
		this.value = value
	}
}



