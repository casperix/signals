package ru.casperix.signals.concrete

class EnumeratedStorageSignal<Value>(first: Value, val variants: Collection<Value>) : StoragePromise<Value> {
	private val signal = StorageSignal(first)
	override var value: Value
		get() = signal.value
		set(value) {
			if (!variants.contains(value)) return
			signal.set(value)
		}


	override fun cancel(slot: Slot): Boolean {
		return signal.cancel(slot)
	}

	override fun then(listener: (Value) -> Unit): Slot {
		return signal.then(listener)
	}

	override fun removeAllListeners() {
		signal.removeAllListeners()
	}

	override fun set(value: Value) {
		this.value = value
	}
}