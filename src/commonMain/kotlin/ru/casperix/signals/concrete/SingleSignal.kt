package ru.casperix.signals.concrete

import ru.casperix.signals.api.AbstractSignal

/**
 * 	Разовый сигнал ожидания события
 * 	Оповещает всех подписчиков при установке
 * 	Аналогичен [Signal] но срабатывает только единожды
 *
 * 	val signal = Signal<String>
 * 	signal.then { println(it) }
 * 	signal.set("Hello-")
 * 	signal.set("World")
 */
class SingleSignal<Event> : SinglePromise<Event>, AbstractSignal<(Event) -> Unit, Slot>(SlotCollection()) {
	override var completed = false; private set
	private var value: Event? = null

	override fun then(listener: (Event) -> Unit): Slot {
		if (completed) {
			listener(value!!)
		}
		return super.then(listener)
	}

	override fun set(value: Event) {
		if (!completed) {
			completed = true
			this.value = value

			forEach { listener ->
				listener(value)
			}
		}
	}

	fun dropResult() {
		completed = false
		value = null
	}
}