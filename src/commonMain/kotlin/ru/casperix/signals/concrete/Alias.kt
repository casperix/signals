package ru.casperix.signals.concrete

import ru.casperix.signals.api.*
import kotlin.properties.Delegates
import kotlin.properties.ReadWriteProperty

typealias Future<Event> = CustomFuture<(Event) -> Unit, Slot>
typealias UpdateFuture<Event> = CustomFuture<(Event, Event) -> Unit, Slot>
typealias EmptyFuture = CustomFuture<() -> Unit, Slot>
typealias Storage<Event> = CustomStorage<Event>
typealias Single = CustomSingle
typealias EitherFuture<Accept, Reject> = CustomEitherFuture<(Accept) -> Unit, (Reject) -> Unit, Slot>
typealias EmptyEitherFuture = CustomEitherFuture<() -> Unit, () -> Unit, Slot>

interface Promise<Event> : Future<Event>, CustomPromise<(Event) -> Unit, Slot> {
	fun set(value: Event)

	fun observable(initialValue: Event): ReadWriteProperty<Any?, Event> {
		return Delegates.observable(initialValue) { _, old, new -> if (old != new) set(new) }
	}
}

interface EmptyPromise : EmptyFuture, CustomPromise<() -> Unit, Slot> {
	fun set()

	fun <T> observable(initialValue:T): ReadWriteProperty<Any?, T> {
		return Delegates.observable(initialValue) { _, old, new -> if (old != new) set() }
	}
}

interface UpdatePromise<Event> : CustomPromise<(Event, Event) -> Unit, Slot>, UpdateFuture<Event> {
	fun set(value: Event)
}

interface EitherPromise<Accept, Reject> : EitherFuture<Accept, Reject>, CustomEitherPromise<(Accept) -> Unit, (Reject) -> Unit, Slot> {
	fun accept(accept: Accept)
	fun reject(reject: Reject)
}

interface EmptyEitherPromise : EmptyEitherFuture, CustomEitherPromise<() -> Unit, () -> Unit, Slot> {
	fun accept()
	fun reject()
}

interface SingleFuture<Event> : Future<Event>, CustomSingleFuture<(Event) -> Unit, Slot>
interface SinglePromise<Event> : SingleFuture<Event>, Promise<Event>, CustomSinglePromise<(Event) -> Unit, Slot>
interface EmptySingleFuture : EmptyFuture, CustomSingleFuture<() -> Unit, Slot>
interface EmptySinglePromise : EmptySingleFuture, EmptyPromise, CustomSinglePromise<() -> Unit, Slot>
interface StorageFuture<Event> : Future<Event>, CustomStorageFuture<(Event) -> Unit, Event, Slot>
interface StoragePromise<Event> : StorageFuture<Event>, Promise<Event>, CustomStoragePromise<(Event) -> Unit, Event, Slot>, CustomStorage<Event>

typealias BooleanSwitcher = StoragePromise<Boolean>
