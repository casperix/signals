package ru.casperix.signals.concrete

import ru.casperix.misc.Either
import ru.casperix.misc.fold
import ru.casperix.signals.api.CustomEitherFuture
import ru.casperix.signals.api.CustomEitherPromise
import ru.casperix.signals.api.CustomFuture


/**
 * 	Искомый сигнал срабатывает от любого инициатора
 */
fun <Input, Output> CustomFuture<(Input) -> Unit, Slot>.transform(converter: (Input) -> Output): CustomFuture<(Output) -> Unit, Slot> {
    val signal = Signal<Output>()
    then {
        signal.set(converter(it))
    }
    return signal
}

fun <Accept, Reject, NextAccept, NextReject> EitherFuture<Accept, Reject>.map(
    acceptConverter: (Accept) -> Either<Reject, NextAccept>,
    rejectConverter: (Reject) -> NextReject
): EitherFuture<NextAccept, NextReject> {
    return mapAccept(acceptConverter).mapReject(rejectConverter)
}

fun <Accept, Reject, NextAccept> EitherFuture<Accept, Reject>.mapAccept(acceptConverter: (Accept) -> Either<Reject, NextAccept>): EitherFuture<NextAccept, Reject> {
    val nextSignal = EitherSignal<NextAccept, Reject>()
    then({ accept ->
        acceptConverter(accept).fold({ reject ->
            nextSignal.reject(reject)
        }, { nextAccept ->
            nextSignal.accept(nextAccept)
        })
    }, {
        nextSignal.reject(it)
    })
    return nextSignal
}

fun <Accept, Reject, NextReject> EitherFuture<Accept, Reject>.mapReject(rejectConverter: (Reject) -> NextReject): EitherFuture<Accept, NextReject> {
    val nextSignal = EitherSignal<Accept, NextReject>()
    then({
        nextSignal.accept(it)
    }, {
        nextSignal.reject(rejectConverter(it))
    })
    return nextSignal
}