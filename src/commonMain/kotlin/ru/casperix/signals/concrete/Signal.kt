package ru.casperix.signals.concrete

import ru.casperix.signals.api.AbstractSignal

/**
 * 	При срабатывании разово уведомляет всех подписчиков
 */
class Signal<Event>() : Promise<Event>, AbstractSignal<(Event) -> Unit, Slot>(SlotCollection()) {
	override fun set(value: Event) {
		forEach { listener ->
			listener(value)

		}
	}

	companion object {
		fun <Event> pipe(vararg initiatorList: Future<Event>): Future<Event> {
			val signal = Signal<Event>()
			initiatorList.forEach { initiator ->
				initiator.then { event ->
					signal.set(event)
				}
			}
			return signal
		}
	}
}

